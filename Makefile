
all: upload-blocker.xpi

clean:
	rm -f *.xpi

upload-blocker.xpi: *.json *.js
	zip --quiet -0 -r - *.json *.js > $@

.PHONY: all clean
