/*
 * GLOBAL VARIABLES
 */
var DEBUG = 0;
var LOG = function() {
  if (DEBUG) {
    var args = ['upload-blocker:'].concat(arguments);
    console.log.apply(console, args);
  }
};

/*
 * Return true if the element is a file input.
 */
function isFileInput(elm) {
  var tag = elm.tagName.toLowerCase();
  var type = elm.type;

  return tag === 'input' && type == 'file';
}

/*
 * Disable all file inputs in the document.
 */
function disableFileInputAll() {
  var list = document.querySelectorAll('input[type=file]');

  for (var i = 0; i < list.length; i++) {
    var input = list[i];
    if (!input.disabled) {
      LOG('Disable a file input', input);
      input.disabled = true;
    }
  }
}

/*
 * Detect if we need to trigger disableFileInput().
 *
 * The arg `mutationList` is an array of <MutationRecord>. See:
 * https://developer.mozilla.org/ja/docs/Web/API/MutationRecord
 */
function shouldFire(mutationList) {
  for (var i = 0; i < mutationList.length; i++) {
    var mutation = mutationList[i];
    var target = mutation.target;

    // When a tree of elements is added to the document.
    if (mutation.type === 'childList') {
      return true;
    }

    // When the disabled attribute of an element is modified.
    if (mutation.type === 'attributes') {
      if (isFileInput(target) && !target.disabled) {
        return true;
      }
    }
  }
  return false;
}

document.addEventListener('DOMContentLoaded', function onLoad(){
  document.removeEventListener('DOMContentLoaded', onLoad);

  LOG('Start a watcher in a content process')

  var observer = new MutationObserver(function (mutationList){
    LOG('MutationEvent', mutationList);
    if (shouldFire(mutationList)) {
      disableFileInputAll();
    }
  });

  observer.observe(document.body, {
    attributes: true,
    childList: true,
    subtree: true,
    attributeFilter: ["disabled"]
  });

  disableFileInputAll();
});
